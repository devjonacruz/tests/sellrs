<?php

namespace App\Services\Ecommerces;

use App\Interfaces\Ecommerce;
use App\Models\Product;

class Vtex implements Ecommerce
{
    public function getProducts(): array
    {
        return [
            [
                "id" => "T001",
                "title" => "T-shirt M",
                "price" => 24.99,
                "currency" => "GBP",
                "details" => [
                    "image" => "http://image",
                    "description" => "Medium t-shirt",
                ],
            ],
            [
                "id" => "J001",
                "title" => "Jean 38",
                "price" => 100.99,
                "currency" => "GBP",
                "details" => [
                    "image" => "http://image-jean",
                    "description" => "Best quality",
                ],
            ],
        ];
    }

    public function import(array $products): bool
    {
        print_r("Importando Vtex\n");

        foreach ($products as $key => $product) {
            // Product::create([
            //     'name' => $product['title'],
            //     'description' => $product['details']['description'],
            //     'price' => $product['price'],
            //     'currency' => $product['currency'],
            //     'image' => $product['details']['image'],
            //     'sku' => $product['id'],
            //     'reference' => $product['id'],
            // ]);
        }

        return true;
    }
}
