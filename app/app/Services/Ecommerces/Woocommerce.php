<?php

namespace App\Services\Ecommerces;

use App\Interfaces\Ecommerce;
use App\Models\Product;

class Woocommerce implements Ecommerce
{
    public function getProducts(): array
    {
        return [
            [
                "id" => "T001",
                "item_title" => "T-shirt M",
                "price" => "24.99 GBP",
                "main_image" => "http://image",
                "description" => "Medium t-shirt",
            ],
            [
                "id" => "K738",
                "item_title" => "Sweater",
                "price" => "44.99 GBP",
                "main_image" => "http://sweater",
            ],
        ];
    }

    public function import(array $products): bool
    {
        print_r("Importando Woocommerce\n");

        foreach ($products as $key => $product) {
            // Product::create([
            //     'name' => $product['item_title'],
            //     'description' => $product['description'],
            //     'price' => $product['price'],
            //     'currency' => 'COP',
            //     'image' => $product['main_image'],
            //     'sku' => $product['id'],
            //     'reference' => $product['id'],
            // ]);
        }

        return true;
    }
}
