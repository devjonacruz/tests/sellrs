<?php

namespace App;

use App\Interfaces\Ecommerce;

class Product
{
    protected $ecommerce;

    public function setEcommerce(Ecommerce $ecommerce): void
    {
        $this->ecommerce = $ecommerce;
    }

    public function __call($name, $arguments)
    {
        $classname = "App\Services\Ecommerces\\" . ucfirst($name);

        $this->setEcommerce(new $classname);

        return $this->ecommerce;
    }

    public function import(Ecommerce $ecommerce)
    {
        return $ecommerce->import($ecommerce->getProducts());
    }
}
