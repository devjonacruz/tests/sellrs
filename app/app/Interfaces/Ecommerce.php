<?php

namespace App\Interfaces;

interface Ecommerce
{
    public function getProducts(): array;
    public function import(array $products): bool;
}
