<?php

use App\Product as ProductClass;
use App\Services\Ecommerces\Vtex;
use PHPUnit\Framework\TestCase;

class ProductTest extends TestCase
{
    private $cl;

    public function setUp(): void
    {
        $this->cl = new ProductClass();
    }

    public function testImportProductWithOutParams()
    {
        $this->expectException(ArgumentCountError::class);
        $this->cl->import();
    }

    public function testImportProductWithInvalidParams()
    {
        $this->expectException(TypeError::class);
        $this->cl->import([]);
    }

    public function testImportProductWithParams()
    {
        $this->assertEquals(true, $this->cl->import(new Vtex()));
    }
}
