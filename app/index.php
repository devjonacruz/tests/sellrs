<?php

use App\Product;
use App\Services\Ecommerces\Vtex;
use App\Services\Ecommerces\Woocommerce;

require "vendor/autoload.php";

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

if (!function_exists('env')) {
    function env($key, $default = null)
    {
        return isset($_ENV[$key]) ? $_ENV[$key] : $default;
    }
}

$capsule = new Illuminate\Database\Capsule\Manager;

$capsule->addConnection([
    "driver" => "mysql",
    "host" => env('_DB_HOST'),
    "database" => env('_DB_NAME'),
    "username" => env('_DB_USER'),
    "password" => env('_DB_PASS')
]);
$capsule->setAsGlobal();
$capsule->bootEloquent();


$product = new Product();

$product->import(new Vtex());
$product->import(new Woocommerce());
