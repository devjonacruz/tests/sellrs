The tests
The requirement is to build a system that, given products from different source providers will
import them into a normalized structure. You are free to choose how to store and represent
the data.
A database or any other external dependency is not required for this test.
Considerations:
- Sources could have different data structure and format
- Source data could be missing attributes for standardized structure
- Same product could be in different providers

Los examenes
El requisito es construir un sistema que, dados los productos de diferentes proveedores de origen,
importarlos en una estructura normalizada. Usted es libre de elegir cómo almacenar y representar
los datos.
No se requiere una base de datos ni ninguna otra dependencia externa para esta prueba.
Consideraciones:
- Las fuentes pueden tener diferente estructura y formato de datos
- A los datos de origen podrían faltar atributos para la estructura estandarizada
- El mismo producto podría estar en diferentes proveedores
